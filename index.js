const inquirer = require('inquirer');
const { parseCommand } = require('./src/commands');
const { Canvas } = require('./src/canvas');

const CREATE_CANVAS_COMMAND = 'C';
const DRAW_LINE_COMMAND = 'L';
const DRAW_RECTANGLE_COMMAND = 'R';
const FILL_AREA_COMMAND = 'B';
const QUIT_COMMAND = 'Q';

let canvas;

/**
 * Process user command
 *
 * @param {String} commandString - user command string
 */
const processCommand = ({ command: commandString }) => {
  const { command, coordinates, color } = parseCommand(commandString);
  if (!canvas && ![CREATE_CANVAS_COMMAND, QUIT_COMMAND].includes(command)) {
    console.warn('You have to initialise canvas fist!');
    return;
  }
  switch (command) {
    case CREATE_CANVAS_COMMAND:
      canvas = new Canvas(...coordinates);
      break;
    case DRAW_LINE_COMMAND:
      canvas.drawLine(coordinates);
      break;
    case DRAW_RECTANGLE_COMMAND:
      canvas.drawRectangle(coordinates);
      break;
    case FILL_AREA_COMMAND:
      canvas.fillArea(coordinates, color);
      break;
    case QUIT_COMMAND:
      process.exit();
      break;
    default:
      console.warn('Unknown command');
      return;
  }
  canvas.drawCanvas(canvas);
};

/**
 * Ask user for command
 */
const askForCommand = async () => {
  try {
    const command = await inquirer.prompt([{
      type: 'input',
      name: 'command',
      message: 'enter command',
    }]);
    processCommand(command);
    await askForCommand();
  } catch (error) {
    console.error(error);
  }
};

askForCommand();
