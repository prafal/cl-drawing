const BORDERS_WIDTH = 2;

/**
 * Class representing a canvas
 */
class Canvas {
  /**
   * Create a canvas
   *
   * @param {Number} width - canvas width
   * @param {Number} height - canvas height
   */
  constructor(width = 0, height = 0) {
    this.width = width + BORDERS_WIDTH;
    this.height = height + BORDERS_WIDTH;
    this.canvas = Array.from(Array(this.height), () => new Array(this.width));
    this.initiateCanvasWithBorder();
  }

  /**
   * Initiate canvas with border
   */
  initiateCanvasWithBorder() {
    for (let y = 0; y < this.height; y += 1) {
      for (let x = 0; x < this.width; x += 1) {
        if (y === 0 || y === this.height - 1) {
          this.canvas[y][x] = '-';
        } else if (x === 0 || x === this.width - 1) {
          this.canvas[y][x] = '|';
        } else {
          this.canvas[y][x] = ' ';
        }
      }
    }
  }

  /**
   * Draw canvas in terminal
   */
  drawCanvas() {
    for (let y = 0; y < this.height; y += 1) {
      console.log(this.canvas[y].join(''));
    }
  }

  /**
   * Draw line on canvas
   *
   * @param {Number[]} coordinates - points coordinates in order x1, y1, x2, y2
   */
  drawLine([x1, y1, x2, y2]) {
    if (!(x1 === x2 || y1 === y2)) {
      console.warn('Only horizontal or vertical lines are supported');
      return;
    }
    const xs = [x1, x2].sort();
    const ys = [y1, y2].sort();
    for (let y = ys[0]; y <= ys[1]; y += 1) {
      for (let x = xs[0]; x <= xs[1]; x += 1) {
        this.fillCell(x, y, 'x');
      }
    }
  }

  /**
   * Draw rectangle on canvas with upper left corner (x1,y1) and lower right corner (x2,y2)
   *
   * @param {Number[]} coordinates - points coordinates in order x1, y1, x2, y2
   */
  drawRectangle([x1, y1, x2, y2]) {
    const xs = [x1, x2].sort();
    const ys = [y1, y2].sort();
    for (let y = ys[0]; y <= ys[1]; y += 1) {
      for (let x = xs[0]; x <= xs[1]; x += 1) {
        if (y === y1 || y === y2 || x === x1 || x === x2) {
          this.fillCell(x, y, 'x');
        }
      }
    }
  }

  /**
   * Fill cell with given color
   *
   * @param {Number} x - point x coordinate
   * @param {Number} y - point y coordinate
   * @param {String} color - color to fill the cell
   */
  fillCell(x, y, color) {
    if (this.validateCoordinates(x, y)) {
      this.canvas[y][x] = color;
    }
  }

  /**
   * Fill area with given color
   *
   * @param {Number[]} coordinates - point coordinates in order x1, y1
   * @param {String} color - color to fill the area
   */
  fillArea([x, y], color) {
    if (this.validateCoordinates(x, y)) {
      const currentColour = this.canvas[y][x];
      if (currentColour !== color) {
        this.floodFill(x, y, color, currentColour);
      }
    }
  }

  /**
   * Fill all cells connected to given point
   *
   * @param {Number} x - point x coordinate
   * @param {Number} y - point y coordinate
   * @param {String} color - color to fill the area
   * @param {String} currentColour - color that fills the area
   */
  floodFill(x, y, color, currentColour) {
    if (!this.validateCoordinates(x, y)) {
      return;
    }
    if (this.canvas[y][x] !== currentColour) {
      return;
    }
    this.fillCell(x, y, color);

    this.floodFill(x - 1, y, color, currentColour);
    this.floodFill(x + 1, y, color, currentColour);
    this.floodFill(x, y - 1, color, currentColour);
    this.floodFill(x, y + 1, color, currentColour);
  }

  /**
   * Validate coordinates
   *
   * @param {Number} x - point x coordinate
   * @param {Number} y - point y coordinate
   * @returns {boolean} true if coordinates are valid
   */
  validateCoordinates(x = 0, y = 0) {
    return !(x <= 0 || x >= this.width || y <= 0 || y >= this.height);
  }
}

module.exports = {
  Canvas,
};
