/**
 * Parse user string command
 *
 * @param {String} commandString - user command string
 * @returns {{color: any, coordinates: number[], command: string}}
 */
const parseCommand = (commandString) => {
  const parsed = commandString.split(' ').filter(Boolean);
  const command = parsed[0];

  const coordinates = parsed
    .filter((argument) => Number.isInteger(+argument))
    .map((argument) => Number.parseInt(argument, 10));
  const color = parsed[parsed.length - 1];
  return {
    command: command.toUpperCase(),
    coordinates,
    color,
  };
};

module.exports = {
  parseCommand,
};
